import os
import random
from gi.repository import Gtk

class MainApp:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join('glade','main_window.glade'))
        self.builder.connect_signals(self)

        grid = self.builder.get_object('fridge_grid')
        self.vertical = os.path.join('icons','vertical.png')
        self.horizontal = os.path.join('icons','horizontal.png')

        size = 4
        boxes = [Gtk.EventBox() for i in range(size*size)]
        i = 0
        for box in boxes:
            box.icon = Gtk.Image()
            box.icon.hor_flag = random.randint(0,1)==0
            box.icon.set_from_file(self.horizontal if box.icon.hor_flag else self.vertical)
            box.add(box.icon)
            box.connect('button-press-event',self.onImageClick)
            box.x = i // size
            box.y = i % size
            i += 1
            grid.attach(box,box.x,box.y,1,1)

        window = self.builder.get_object('main_window')
        window.connect('destroy',Gtk.main_quit)
        window.set_resizable(False)
        window.show_all()

        self.boxes = boxes

    def onImageClick(self,widget,event):
        for box in self.boxes:
            if box.x == widget.x or box.y == widget.y:
                self.changeIcon(box.icon)
        if all([box.icon.hor_flag for box in self.boxes ]):
            dialog = Gtk.MessageDialog(None,0,Gtk.MessageType.INFO,Gtk.ButtonsType.CLOSE, "You are win!")
            dialog.run()
            for box in self.boxes:
                box.icon.hor_flag = random.randint(0,1)==0
                box.icon.set_from_file(self.horizontal if box.icon.hor_flag else self.vertical)
            dialog.destroy()

    def changeIcon(self,icon):
        icon.hor_flag = not(icon.hor_flag)
        icon.set_from_file(self.horizontal if icon.hor_flag else self.vertical)
       
if __name__=='__main__':
    MainApp()
    Gtk.main()

